#include "main.h"

int32_t main(void) {
    const vt_version_t v = vt_version_get();
    printf("Vita version: %s\n", v.str);
    return 0;
}

